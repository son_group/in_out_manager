import { combineReducers } from "redux";
import { EmployeeRequestReducer } from "./EmployeeRequestReducer";
import { userReducer } from "./reducerUser";

export const rootReducer = combineReducers({
  EmployeeRequestReducer: EmployeeRequestReducer,
  userReducer: userReducer,
});

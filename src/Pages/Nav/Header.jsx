import React from "react";
import UserNav from "./UserNav";

export default function Header() {
  return (
    <div className="shadow">
      <div className="container flex justify-between mt-3">
        <img className="mb-3" src="./img/logo.png" alt="Mani Logo" />
        <UserNav />
      </div>
    </div>
  );
}

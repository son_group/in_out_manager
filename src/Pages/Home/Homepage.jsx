import React from "react";
import { useSelector } from "react-redux";
import { Link, NavLink, Route, Routes } from "react-router-dom";
import Login from "../Login/Login";
import Header from "../Nav/Header";
import Layout from "../Nav/Layout";
import EmployeeRequest from "../Person/Users/EmployeeRequest";

export default function Homepage() {
  let user = useSelector((state) => {
    // console.log("user info, ", state.userReducer.userInfo.data.userID);
    return state.userReducer.userInfo;
  });

  let renderContent = () => {
    if (user) {
      return (
        <div>
          <Header />
          <EmployeeRequest />
        </div>
      );
    } else {
      return <Login />;
    }
  };

  return <div>{renderContent()}</div>;
}

import React, { useEffect, useState } from "react";
import { personService } from "../../../services/personService";

export default function InOutRequestList() {
  const [requestList, setRequestList] = useState([]);
  useEffect(() => {
    personService
      .getPersonRequestList()
      .then((res) => {
        console.log("list request", res);
        setRequestList(res.data);
      })
      .catch((err) => {
        console.log("error ", err);
      });
  }, []);

  const handleDeleteRequest = (requestID) => {
    let index = requestList.findIndex((request) => {
      return request.requestID === requestID;
    });
    if (index !== -1) {
      let request = requestList[index];
      this.props.deleteRequest(request);
    }

    console.log("index cua vi tri delete", index);
  };

  const handleEditRequest = (requestID) => {
    let index = requestList.findIndex((request) => {
      return request.requestID === requestID;
    });
    if (index !== -1) {
      let request = requestList[index];
      this.props.editRequest(request);
    }

    console.log("index cua vi tri edit", index);
  };

  const renderRequest = () => {
    return requestList.map((request, index) => {
      return (
        <tr key={index}>
          <td>{request.employeeID}</td>
          <td>{request.requestType}</td>
          <td>{request.requestReason}</td>
          <td>{request.requestDate}</td>
          <td>{request.requestTime}</td>
          <td>{request.requestStatus}</td>
          <td>
            <div>
              <button
                onClick={() => {
                  handleDeleteRequest(request.requestID);
                }}
                type="button"
                className="btn "
              >
                <i className="fa fa-trash" />
              </button>
              <button
                onClick={() => {
                  handleEditRequest(request.requestID);
                }}
                type="button"
                className="btn "
              >
                <i className="fa fa-pen-square" />
              </button>
            </div>
          </td>
        </tr>
      );
    });
  };

  return (
    <div className="container py-5">
      <table className="table">
        <thead>
          <tr>
            <th>Mã nhân viên</th>
            <th>Loại yêu cầu</th>
            <th>Lý do</th>
            <th>Ngày xin</th>
            <th>Thời gian</th>
            <th>Trạng thái</th>
            <th>Cập nhật</th>
          </tr>
        </thead>
        <tbody>{renderRequest()}</tbody>
      </table>
    </div>
  );
}

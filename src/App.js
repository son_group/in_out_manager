import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import Homepage from "./Pages/Home/Homepage";
import Login from "./Pages/Login/Login";
import Layout from "./Pages/Nav/Layout";
import EmployeeRequest from "./Pages/Person/Users/EmployeeRequest";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Homepage />} />
        <Route
          path="/request"
          element={<Layout Component={EmployeeRequest} />}
        />
        <Route path="/login" element={<Login />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;

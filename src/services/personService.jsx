import axios from "axios";
import { BASE_URL } from "./configURL";

export const personService = {
  getPersonRequestList: () => {
    return axios({
      url: `${BASE_URL}/in_out_person`,
      method: "GET",
    });
  },
};
